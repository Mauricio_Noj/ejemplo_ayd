provider "google" {
    project     = "sa-proyecto-420423"
    credentials = file("/home/mauricio/Documentos/ejemplo_ayd/ejemplo_ayd/key/key.json")
    region      = "us-central1"
    zone        = "us-central1-c"
}

resource "tls_private_key" "dev_ssh_key" {
    algorithm = "RSA"
    rsa_bits  = 2048
}

resource "local_file" "private_key_file" {
    content  = tls_private_key.dev_ssh_key.private_key_pem
    filename = "${path.module}/dev_ssh_key.pem"
    file_permission = "0400"
}

resource "google_compute_instance" "dev_instance" {
    name         = "dev-instance"
    machine_type = "n1-standard-1"
    zone         = var.zone
    allow_stopping_for_update = true

    boot_disk {
        initialize_params {
            image = "ubuntu-2004-focal-v20240307b"
        }
    }

    network_interface {
        network = "default"
        access_config {}
    }

    metadata = {
        "ssh-keys" = "develop:${tls_private_key.dev_ssh_key.public_key_openssh}"
    }

provisioner "remote-exec" {
    connection {
        type        = "ssh"
        host        = self.network_interface[0].access_config[0].nat_ip
        user        = "develop"
        private_key = tls_private_key.dev_ssh_key.private_key_pem
    }
    inline = [
        "sudo DEBIAN_FRONTEND=noninteractive apt-get update",
        "sudo DEBIAN_FRONTEND=noninteractive apt-get upgrade -y",
        "sudo DEBIAN_FRONTEND=noninteractive apt-get install ansible -y"

    ]
}


    provisioner "file" {
        source      = "/home/mauricio/Documentos/ejemplo_ayd/ejemplo_ayd/ansible"
        destination = "/home/develop/ansible"

        connection {
            type        = "ssh"
            host        = self.network_interface[0].access_config[0].nat_ip
            user        = "develop"
            private_key = tls_private_key.dev_ssh_key.private_key_pem
        }
    }
}

