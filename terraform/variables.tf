variable "zone" {
  description = "The zone to deploy the VM instance"
  type        = string
  default     = "us-central1-c"  // Puedes cambiar este valor por defecto a la zona que prefieras
}
